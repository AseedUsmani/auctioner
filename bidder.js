  const express = require('express');
  const app = express();
  var request = require('request');
  const http = require('http').Server(app);
  var bodyParser = require('body-parser');
  app.use(bodyParser.urlencoded({extended: false}));
  app.use(bodyParser.json());


  const BID_URL = '/bid';

  class Bidder {
    constructor(config) {
      /** Port on which the bidder should listen for bid request */
      this.port = config.port;

      /** Address to register with auctioner */
      this.registerWithAuctionerURL = config.registerWithAuctionerURL;

      /** Waiting time in ms after which bidder should respond to bid request  */
      this.bidResponseTime = config.bidResponseTime;
    }

    /**
     * Returns host
     */
    getHost() {
      // TODO: Use system ip instead of localhost in bid url
      return `http://0.0.0.0:${this.port}`;
    }

    /**
     * Starts the bidder by 
     * a) registering itself with autioner
     * b) registering all the routes
     */
    start() {
      this.registerRoutes();
      this.registerWithAuctioner();
    }

    /**
     * Registers itself with auctioner
     */
    registerWithAuctioner() {
      var body = {
        method: 'POST',
        body: JSON.stringify({
          bidURL: this.getHost() + BID_URL,
        })
      }
      request({
        url: this.registerWithAuctionerURL,
        method: "POST",
        json: true,
        body: body
      }, function (error, response, body){
            // TODO: Receive auth token and save... 
            console.log('Registered')
      });
    }

    /**
     * Initializes routes for
     * a) listening for bid requests
     * b) listening for getad requests
     */
    registerRoutes() {
      app.get(BID_URL, this.handleBidRequest.bind(this));

      app.set('port', this.port);

      http.listen(app.get('port'));
    }

    /**
     * Responds to a bid request after waiting for specific time period */
    handleBidRequest(request, response) {
      const auctionId = request.query.auctionId;
      // Generate a random bid amount between 1 and 100
      const bidAmount = parseInt(Math.random() * 100) + 1;
      // Generate a random ad (string of length 5)
      const ad = Math.random().toString(36)
        .replace(/[^a-z]+/g, '').substr(0, 5);
      setTimeout(() => {
        response.send(JSON.stringify({bidAmount, ad}));
      }, this.bidResponseTime);
    }
  }

  /* End class Bidder */


  const [port, auctionerAddress, bidResponseTime] = process.argv.slice(2);
  bidder_config = {
    port: port,
    registerWithAuctionerURL: auctionerAddress,
    bidResponseTime: bidResponseTime
  }

  const bidder = new Bidder(bidder_config);
  bidder.start();

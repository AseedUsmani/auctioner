# Auctioner

## Setup
`git clone git clone https://gitlab.com/AseedUsmani/auctioner.git`

`cd Auctioner`

`npm install`

## Run

### Auctioner:
`node auctioner.js <port_number>`

### Bidder:
`node bidder.js <port_number> <auctioner_URL>/register <delay in ms>`

### Example:
```
x=8000
node auctioner.js $x
node bidder.js 8001 http://localhost:$x/register 50
node bidder.js 8002 http://localhost:$x/register 410 # this bidder will not respond in time
node bidder.js 8003 http://localhost:$x/register 100
# PS: You can run as many instances of the bidder as you want, as long as they're on different ports.
```

### Creating bids
Send a `GET` request on `<auctioner_URL>/getAd`. For the above example, the URL will be `http://localhost:8000/getAd`. The server will return a JSON with a string signifying an advertisement and a cost of the advertisement.

### Docker support:
This feature not complete yet.
### Design changes required to make the app run on docker:
Auctioner runs fine. To run it, create an image using the included Dockerfile, run the container and bind the container port with a host port.

In bidder, instead of `http://localhost:<port>`, while registering the bidder, instead of sending the localhost URL, send `<host_ip>:<host_port>/bid` where `host_ip` is the ip of the host the container is running and `host_port` is the port binded to the container port. 
const express = require('express');
const app = express();
var req = require('request');
const http = require('http').Server(app);
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

const REGISTER_URL = '/register';
const AD_REQUEST_URL = '/getAd';

class Auctioner {
  constructor(config) {
    /** Port on which the bidder should listen */
    this.port = config.port;

    /** Waiting time in ms after which auctioner should respond to ad request  */
    this.adResponseTime = 200;

    /** Bidders registerd with auctioner */
    this.bidders = new Set(); // set helps in avoiding duplicate bidders
  }

  /**
   * Returns host
   */
  getHost() {
    // TODO: Use system ip instead of localhost in bid url
    return `http://0.0.0.0:${this.port}`;
  }

  /**
   * Starts the bidder by 
   * a) registering itself with auctioner
   * b) registering all the routes
   */
  start() {
    this.registerRoutes();
  }

  /**
   * Initializes routes for
   * a) listening for bid requests
   * b) listening for getAd requests
   */
  registerRoutes() {
    // Route to listen to register request from bidder
    app.post(REGISTER_URL, this.handleRegisterRequest.bind(this));

    // Route to listen to ad request
    app.get(AD_REQUEST_URL, this.handleAdRequest.bind(this));

    app.set('port', this.port);

    http.listen(app.get('port'));
    console.log('Registered routes...')
  }

  /**
   * Registers a new bidder
   */
  handleRegisterRequest(request, response) {
    const bidder = request.body;
    this.bidders.add(JSON.parse(bidder.body).bidURL);
    response.sendStatus(200);
    // TODO: Send a token for authentication
    console.log('Registered new bidder... Total bidders: ', this.bidders)
  }

  /**
   * Responds to a bid request after waiting for specific time period */
  handleAdRequest(request, response) {
    // Create a random auction id
    const auctionId = Math.random().toString(36)
      .replace(/[^a-z]+/g, '').substr(0, 5);

    const bids = [];
    this.bidders.forEach(function(bidder) {    
      req({
        url: bidder,
        method: "GET",
      }, function (error, response, body){
          bids.push(JSON.parse(response.body))
      });})


    // TODO: No need to wait for 200ms if all bidders have replied
    setTimeout(function() {
      if (bids.length == 0) {
      response.sendStatus(404);
      return;
    }
      let maxBid = 0;
      let ad = '';
      bids.forEach(function (bid) {
        if (bid.bidAmount > maxBid) {
          maxBid = bid.bidAmount;
          ad = bid.ad;
        }
      });
      response.send({ amount: maxBid, ad: ad })
    }, this.adResponseTime)
  }
}
/******* END CLASS *********/

const [port] = process.argv.slice(2);
const auctionerConfig = { port };
const auctioner = new Auctioner(auctionerConfig);
auctioner.start();
